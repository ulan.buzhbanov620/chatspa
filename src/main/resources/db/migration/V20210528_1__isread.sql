ALTER TABLE message ADD COLUMN is_read boolean;

ALTER TABLE message ADD COLUMN read_timestamp bigint;

ALTER TABLE message ADD COLUMN is_delivered boolean;

ALTER TABLE message ADD COLUMN delivered_timestamp bigint;