package com.example.spachat.service;

import com.example.spachat.model.Auth;
import com.example.spachat.repository.AuthRepository;
import com.example.spachat.service.interfaces.IAuthService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class AuthService implements IAuthService {

    @Autowired
    private AuthRepository authRepository;

    @Override
    public boolean login(String l, String p) {
        Auth a = authRepository.findByLoginAndPassword(l, p);
        if(a == null) {
            return false;
        }
        else{
            a.setLastLoginTimeStamp(new Date().getTime());
            a.setToken(UUID.randomUUID().toString());
            add(a);
            return true;
        }
    }

    @Override
    public boolean isTokenExist(String t) {
        if(authRepository.existsByToken(t))
        {
            return true;
        }
        return false;
    }


    @Override
    public void add(Auth o) {
        authRepository.save(o);
    }

    @Override
    public List<Auth> getAll() {
        return null;
    }

    @Override
    public Auth update(Auth auth) {
        return null;
    }

    @Override
    public void delete(Auth auth) {

    }
}
