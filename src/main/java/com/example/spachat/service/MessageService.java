package com.example.spachat.service;

import com.example.spachat.model.Chat;
import com.example.spachat.model.Message;
import com.example.spachat.model.User;
import com.example.spachat.repository.ChatRepository;
import com.example.spachat.repository.MessageRepository;
import com.example.spachat.repository.UserRepository;
import com.example.spachat.service.interfaces.IMessageService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class MessageService implements IMessageService {

    private final MessageRepository messageRepository;
    private final ParticipantService participantService;

    @Override
    public void add(Message o) {
        o.setCreatedTimestamp(new Date().getTime());
        messageRepository.save(o);
    }

    @Override
    public List<Message> getAll() {

        return messageRepository.findAll();
    }

    @Override
    public Message update(Message message) {

        Optional<Message> messageDbOptional = messageRepository.findById(message.getId());
        if(messageDbOptional.isPresent()) {
            Message messageDb = messageDbOptional.get();
            messageDb.setText(message.getText());
            messageDb.setUpdatedTimestamp(new Date().getTime());

            return messageRepository.save(messageDb);
        } else {
            return null;
        }
    }

    @Override
    public void delete(Message message) {
        messageRepository.delete(message);
    }

    @Override
    public List<Message> findMessagesByChatId(Long chatId) {
        return messageRepository.findMessagesByChatId(chatId);
    }

    @Override
    public List<Message> findAllByChatId(Long id, Integer size) {

        return messageRepository.findAllByChatId(id, PageRequest.of(1, size, Sort.by(Sort.Direction.DESC, "created_timestamp")).first());
    }

    @Override
    public List<Message> findAllByUserId(Long requserid, Long id, Integer size) {
        return messageRepository.findAllByUserId(id, PageRequest.of(1, size, Sort.by(Sort.Direction.DESC, "created_timestamp")).first());
    }

    @Override
    public Message changeisread(Long userId, Long messageId, boolean status) {
        Message m = messageRepository.findMessageByUserIdAndId(userId, messageId);
        m.setRead(status);
        return m;
    }

    @Override
    public List<Message> getnotdelivered(Long id) {

        return messageRepository.findByChatIdAndDeliveredFalse(id);
    }


}
