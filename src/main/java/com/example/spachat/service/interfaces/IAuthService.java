package com.example.spachat.service.interfaces;

import com.example.spachat.model.Auth;

public interface IAuthService extends IService<Auth> {
    public boolean login(String l, String p);

    boolean isTokenExist(String t);
}
