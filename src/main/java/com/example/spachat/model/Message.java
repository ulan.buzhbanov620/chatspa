package com.example.spachat.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "message")
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "chat_id")
    private Long chatId;

    private String text;

    @Column(name = "created_timestamp")
    private Long createdTimestamp;

    @Column(name = "updated_timestamp")
    private Long updatedTimestamp;

    @Column(name = "is_read")
    private boolean read;

    @Column(name = "read_timestamp")
    private Long readTimeStamp;

    @Column(name = "is_delivered")
    private boolean delivered;

    @Column(name = "delivered_timestamp")
    private Long deliveredTimeStamp;

    public boolean getRead(){
        return read;
    }
}

