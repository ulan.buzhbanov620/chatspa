package com.example.spachat.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "auth")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Auth {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String login;

    private String password;

    @Column(name = "last_login_timestamp")
    private Long lastLoginTimeStamp;

    @Column(name = "user_id")
    private Long userId;

    private String token;
}
