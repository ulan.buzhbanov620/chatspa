package com.example.spachat.controller;

import com.example.spachat.service.interfaces.IAuthService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@AllArgsConstructor
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    private IAuthService authService;

    @RequestMapping("/login")
    public ResponseEntity<?> login(@RequestParam(value = "login") String l, @RequestParam(value = "pass") String p){

        if(authService.login(l, p)){
            return ResponseEntity.ok(l);
        }

        return ResponseEntity.ok("Not Found");
    }


}
