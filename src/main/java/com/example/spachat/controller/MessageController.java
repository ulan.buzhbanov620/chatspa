package com.example.spachat.controller;

import com.example.spachat.model.Message;
import com.example.spachat.service.MessageService;
import com.example.spachat.service.interfaces.IAuthService;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@AllArgsConstructor
@RequestMapping("/api/message")
public class MessageController {
    private final MessageService messageService;

    private final IAuthService authService;

    @GetMapping("/getall")
    public ResponseEntity<?> getAll(@RequestHeader String t) {
        if(authService.isTokenExist(t)){
            return ResponseEntity.ok(messageService.getAll());
        }

        return ResponseEntity.ok("Token is not found!!!");
    }

    @PostMapping("/add")
    public ResponseEntity<?> add(@RequestBody Message message, @RequestHeader String t) {
        Date date = new Date();

        if(authService.isTokenExist(t)){
            message.setCreatedTimestamp(date.getTime()/1000);

            messageService.add(message);

            return ResponseEntity.ok("Message successfully added");
        }
        return ResponseEntity.ok("Token is not found!!!");

    }

    @PutMapping("/edit")
    public ResponseEntity<?> edit(@RequestBody Message message, @RequestHeader String t) {
        if(authService.isTokenExist(t)){
            return ResponseEntity.ok(messageService.update(message));
        }

        return ResponseEntity.ok("Token is not found!!!");
    }

    @DeleteMapping("/delete")
    public ResponseEntity<?> delete(@RequestBody Message message, @RequestHeader String t) {
        if(authService.isTokenExist(t)){
            messageService.delete(message);
            return ResponseEntity.ok("Message successfully deleted");
        }
        return ResponseEntity.ok("Token is not found!!!");
    }

    @GetMapping("/get-messages/{chatId}")
    public ResponseEntity<?> getMessagesByChatId(@PathVariable Long chatId, @RequestHeader String t) {
        if(authService.isTokenExist(t)){
            return ResponseEntity.ok(messageService.findMessagesByChatId(chatId));
        }

        return ResponseEntity.ok("Token is not found!!!");
    }

//    @GetMapping("/getLast10ByChatId/{id}")
//    public ResponseEntity<?> getlast10bychatid(@PathVariable("id") Long id){
//
//        return ResponseEntity.ok(messageService.getLast10ByChatId(id));
//    }
//
//    @GetMapping("/getLast10ByUserId/{id}")
//    public ResponseEntity<?> getlast10byuserid(@PathVariable("id") Long id){
//
//        return ResponseEntity.ok(messageService.getLast10ByUserId(id));
//    }

    @RequestMapping("/getMessageByChatId/{id}")
    public ResponseEntity<?> getMessagesByChatId(@PathVariable Long id, @RequestParam(value = "size", required = false) Integer size, @RequestHeader String t){
        if(authService.isTokenExist(t)){
            return ResponseEntity.ok(messageService.findAllByChatId(id, size));
        }

        return ResponseEntity.ok("Token is not found!!!");
    }

    @RequestMapping("/getMessageByUserId/{id}")
    public ResponseEntity<?> getMessagesByUserId(@PathVariable Long id, @RequestParam(value = "size", required = false) Integer size, @RequestHeader("Request user id") Long requserid, @RequestHeader String t){
        if(authService.isTokenExist(t)){
            return ResponseEntity.ok(messageService.findAllByUserId(requserid, id, size));
        }

        return ResponseEntity.ok("Token is not found!!!");
    }

    @RequestMapping("/isread")
    public ResponseEntity<?> changeisread(@RequestBody Message message, @RequestHeader String t){
        if(authService.isTokenExist(t)){
            return ResponseEntity.ok(messageService.changeisread(message.getUserId(), message.getId(), message.getRead()));
        }

        return ResponseEntity.ok("Token is not found!!!");
    }

    @RequestMapping("/getnotdelivered/{chatid}")
    public ResponseEntity<?> getnotdelivered(@PathVariable Long chatid, @RequestHeader String t){
        if(authService.isTokenExist(t)){
            return ResponseEntity.ok(messageService.getnotdelivered(chatid));
        }

        return ResponseEntity.ok("Token is not found!!!");
    }
}
